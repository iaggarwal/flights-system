export class ApiService {
    static getLivePrice(request) {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return fetch('http://localhost:4000/api/search', {
            method: "POST",
            headers: headers,    
            body: JSON.stringify(request)
        });
    }
}