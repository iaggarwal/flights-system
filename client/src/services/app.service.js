export class AppService {
    static getNextMonday(date) {
        return this.getDateAfterOffset(date, 7 - date.getDay() + 1)
    }

    static getDateAfterOffset(date, offset) {
        let dateCopy = new Date(date.getTime());
        dateCopy.setDate(dateCopy.getDate() + offset);
        return dateCopy;
    }

    static transformDateToApiFormat(date) {
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();   
    }

    static getTimeFromDate(dateAsString) {
        let date = new Date(dateAsString);
        let hour = date.getHours()
        let minutes = date.getMinutes();
        return  (hour < 10 ? '0'+hour : hour) + ':' + (minutes < 10 ? '0'+minutes : minutes);
    }
}