 import { AppService } from './app.service';

describe('AppService: testing application utilities', () => {
    it('AppService: getNextMonday', () => {
        let date = new Date('11-08-2017');
        let nextMonday = AppService.getNextMonday(date);
        expect(nextMonday).toEqual(new Date('2017-11-13T00:00:00.000Z'));
    });

    it('AppService: getDateAfterOffset', () => {
        let date = new Date('11-08-2017')
        let offsetDate = AppService.getDateAfterOffset(date, 1);
        expect(offsetDate).toEqual(new Date('2017-11-09T00:00:00.000Z'));
    });

    it('AppService: transformDateToApiFormat', () => {
        let date = new Date('11-08-2017')
        let apiDate = AppService.transformDateToApiFormat(date);
        expect(apiDate).toEqual('2017-11-8');
    });

    it('AppService: getTimeFromDate', () => {
        let date = new Date('11-08-2017 13:30:45')
        let time = AppService.getTimeFromDate(date);
        expect(time).toEqual('13:30');
    });
});