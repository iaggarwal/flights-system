import React, {Component} from 'react';
import './header.scss';

class Header extends Component {
    render() {
        let outbound = this.props.outbound;
        let inbound = this.props.inbound;
        let numTravellers = this.props.numTravellers;
        let flightClass = this.props.flightClass;
        return (
            <div className='headerRibbon'>
                <div className='headerContent'>
                    <div className='headerTextPrimary'>{outbound} -> {inbound}</div>
                    <div>{numTravellers} travellers, {flightClass}</div>
                </div>
            </div>
        );  
    }
}

export default Header;
