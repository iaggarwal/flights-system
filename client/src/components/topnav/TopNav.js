import React from 'react';
import './TopNav.scss';
import logo from '../..//logo.svg';

const TopNav = () => (
  <header className='header'>
    <a href="/">
      <img className='logo' alt="Skyscanner" src={logo}/>
      <span className='logoText'>Skyscanner</span>
    </a>
    <div className='burger-menu'>&#9776; </div>
  </header>
);

export default TopNav;
