import React, {Component} from 'react';
import './controlBar.scss'

class ControlBar extends Component {
    render() {
        return (
            <div className='control-bar'>
                <div className="left-items">
                    <div className='item left'>Filter</div>
                    <div className='item left'>Sort</div>
                </div>
                <div className="right-items">
                    <div className='item right'>Price alerts</div>
                </div>
            </div>
        );  
    }
}

export default ControlBar;
