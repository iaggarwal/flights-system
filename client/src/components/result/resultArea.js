import React, {Component} from 'react';
import ResultTile from './resultTile';
import './results.scss';

class ResultArea extends Component {
    render() {
        let resultItems = [];
        if (this.props.pricingdata) {
            for (var index = 0; index < 10; index++) {
                let pricingInstance = this.props.pricingdata[index]
                resultItems.push(<ResultTile key={index} pricinginstance={pricingInstance}/>);
            }
        }
        return (
            <div className='resultArea'>
                {resultItems}
            </div>
        );  
    }
}

export default ResultArea;
