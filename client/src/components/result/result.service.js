export class ResultService {
    static getStops(stops) {
        if (stops.length === 0) {
            return "Direct"
        } else if(stops.length === 1){
            return "1 Stop"
        } else {
            return "1+ Stop"
        }
    }

    static getTimeFromDuration(duration) {
        let hour = Math.floor(duration/60);
        let minutes = duration % 60;

        return (hour < 10 ? '0'+hour : hour) + ':' + (minutes < 10 ? '0'+minutes : minutes);
    }
}