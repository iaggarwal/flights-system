import React, {Component} from 'react';
import {AppService} from '../../services/app.service';
import {ResultService} from './result.service';

class ResultTile extends Component {

    getLegDisplay(leg) {
        return (
            <div className='leg'>
                <img className='icon' src={leg.Carriers[0].ImageUrl}/>
                <div className='left flight-details'>
                    <div>{AppService.getTimeFromDate(leg.Departure)}</div>
                    <div className="text-muted place">{leg.OriginStation.Code}</div>
                </div>
                <div className='arrow-icon left text-muted'>&#8594;</div>
                <div className='left flight-details'>
                    <div>{AppService.getTimeFromDate(leg.Departure)}</div>
                    <div className="text-muted place">{leg.DestinationStation.Code}</div>
                </div>
                <div className='right'>
                    <div className="text-muted">{ResultService.getTimeFromDuration(leg.Duration)}</div>
                    <div className="text-success">{ResultService.getStops(leg.Stops)}</div>
                </div>
            </div>
        );
    }

    render() {
        let resultItem = this.props.pricinginstance;
        return (
            <div className='resultItem shadow'>
                {this.getLegDisplay(resultItem.outboundLeg)}
                {this.getLegDisplay(resultItem.inboundLeg)}
                <div className='pricing'>
                    <div className="left">
                        <div className="price">£{resultItem.pricingOptions[0].price}</div>
                        <a className='external-link text-muted left'>{resultItem.pricingOptions[0].agent.Name}</a>
                    </div>
                    <a target="_blank" href={resultItem.pricingOptions[0].deeplinkUrl} className='right button-primary'>Select</a>
                </div>
            </div>
        );  
    }
}

export default ResultTile;
