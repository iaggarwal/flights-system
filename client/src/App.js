import React, { Component } from 'react';
import './App.scss';

import TopNav from './components/topnav';
import Header from './components/header/header';
import ControlBar from './components/controlbar/controlBar';
import ResultArea from './components/result/resultArea';

import { AppService } from './services/app.service';
import { ApiService } from './services/api.service';

class App extends Component {

  constructor() {
    super();
    this.state = {};
  }

  componentWillMount() {

    let fromDate = AppService.getNextMonday(new Date());
    let toDate = AppService.transformDateToApiFormat(AppService.getDateAfterOffset(fromDate, 1));
    fromDate = AppService.transformDateToApiFormat(fromDate);

    this.setState({
        pricingRequest: {
          adults: 1,
          class: "Economy",
          toPlace: "LHR",
          toDate: toDate,
          fromPlace: "EDI",
          fromDate: fromDate,
        }
    });
  }
  componentDidMount() {
    this.getLivePricing();
  }

  getLivePricing() {
    let self = this;
    ApiService.getLivePrice(self.state.pricingRequest)
    .then((response) => response.json())
    .then((results) => {
      self.setState({pricingData: results});
    })
    .catch(console.error);
  }

  render() {
    return (
      <div className="App">
        <TopNav/>
        <div className="app-body">
          <Header numTravellers={this.state.pricingRequest.adults} flightClass={this.state.pricingRequest.class} outbound={this.state.pricingRequest.fromPlace} inbound={this.state.pricingRequest.toPlace}/>
          <ControlBar/>
          <ResultArea pricingdata={this.state.pricingData}/>
        </div>
      </div>
    );
  }
}

export default App;
